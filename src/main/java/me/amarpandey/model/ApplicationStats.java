package me.amarpandey.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApplicationStats {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationStats.class);
	private static int userCount;
	private static Object lock = new Object();
	public static final String AccountNo = "12345678";

	private ApplicationStats() {
		throw new RuntimeException("No instance of this class");
	}

	public static void incrementUserCount() {
		synchronized (lock) {
			userCount++;
		}
	}


	public static void decrementUserCount() {
		logger.info("Account number" + AccountNo );
		synchronized (lock) {
			userCount--;
		}
	}

	public static int getUserCount() {
		logger.info("Account number" + AccountNo );
		synchronized (lock) {
			return userCount;
		}
	}
}
